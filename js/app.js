/**
 * Created by ronald8192 on 31/5/2016.
 */

var buApp = angular.module('buApp', ['ngRoute','buControllers']);

buApp.config(function ($routeProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: 'view/home.html',
            controller: 'homeCtrl'
        })

        .when('/aboutus', {
            templateUrl: 'view/aboutus.html',
            controller: 'aboutusCtrl'
        })
        .when('/contact', {
            templateUrl: 'view/contact.html',
            controller: 'contactCtrl'
        })
        .when('/news', {
            templateUrl: 'view/news.html'
        })
        .when('/news/:id', {
            templateUrl: 'view/news.html',
            controller: 'newsCtrl'
        })
        .when('/research', {
            templateUrl: 'view/research.html',
            controller: 'researchCtrl'
        })
        .when('/404', {
            templateUrl: 'view/404.html',
            controller: '404Ctrl'
        })
        //people
        .when('/people', {
            templateUrl: 'view/people/people_list.html',
            controller: 'peopleCtrl'
        })
        .when('/people/cheungsiuyin', {
            templateUrl: 'view/people/cheungsiuyin.html',
            controller: 'personCtrl'
        })
        .when('/people/kimsm', {
            templateUrl: 'view/people/kimsm.html',
            controller: 'personCtrl'
        })
        .when('/people/lauwc', {
            templateUrl: 'view/people/lauwc.html',
            controller: 'personCtrl'
        })
        .when('/people/chenkc', {
            templateUrl: 'view/people/chenkc.html',
            controller: 'personCtrl'
        })
        .when('/people/hongsi', {
            templateUrl: 'view/people/hongsi.html',
            controller: 'personCtrl'
        })
        .when('/people/zhengjinming', {
            templateUrl: 'view/people/zhengjinming.html',
            controller: 'personCtrl'
        })
        .when('/people/zhaoyanan', {
            templateUrl: 'view/people/zhaoyanan.html',
            controller: 'personCtrl'
        })
        .otherwise({
            redirectTo: 'home'
        });
});