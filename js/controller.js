/**
 * Created by ronald8192 on 31/5/2016.
 */


var buControllers = angular.module('buControllers', []);

buControllers.controller('emptyCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('homeCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('peopleCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('personCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {
    $('body').scrollTop(0);
}]);
buControllers.controller('aboutusCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('contactCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);
buControllers.controller('researchCtrl', ['$scope', '$http', '$routeParams',function ($scope, $http, $routeParams) {}]);

buControllers.controller('newsCtrl', ['$scope', '$http', '$routeParams',
    function ($scope, $http, $routeParams) {
        $scope.seminars = [];
        $http({
            method : "GET",
            url : "data/seminars.json"
        }).then(function successCallback(response) {
            if($routeParams.id !== undefined){
                //select the seminar with the id (ref) provided by route params
                $scope.isInDetail = false;
                angular.forEach(response.data, function(value, key) {
                    if($routeParams.id == value.ref){
                        $scope.seminars.push(response.data[key]);
                        $scope.isInDetail = true;
                    }
                });
                if($scope.seminars.length === 0){
                    $scope.error = "Seminar \"" + $routeParams.id + "\" " + "not found. Below is all seminar(s).";
                    $scope.seminars = response.data;
                }
            }else{
                $scope.seminars = response.data;
            }
        }, function errorCallback(response) {
            $scope.error = "Data not available: " + response.status + " " + response.statusText;
        });
    }
]);